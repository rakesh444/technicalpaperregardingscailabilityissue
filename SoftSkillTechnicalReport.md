# In this article i will present different factors by which project can lead to performance and scailing issue.

### Limited physical resources like memory and CPUs etc -
Management of memory and CPU is important in any project. To avoiding performance and scailing issue we will see these things how memory and CPU are working, If any memory is not in use then we should release it. We should use CPU in such a way that CPU should more time in use.

### Complicated database schema - 
Poor design planning can lead to structural problems that would be expensive to unwind once the database has been rolled out. So we should make sure there should be good indexing and schema is designed in such a way that there should not be redundancy and inconsistancy in database.

### Poorly performed database queries - 
As a coach we will see how database queries are performed for accessing data from database and reading data from database. Let us say if we need any particular portion of a database table then we should use database query in such a way that only that portion of table will accessed instead of whole table. We will make sure to use query in efficient way.

### Inefficient caching - 
We will look how caching machenism are implemented for faster executionof memory. In cach memory we should put all that thing which we need most frequently.
